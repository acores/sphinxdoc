FROM ubuntu:22.04

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN apt clean && apt update && apt -y install apt-transport-https wget tar git python3 python3-pip curl sphinx-doc

RUN pip3 install sphinx sphinx-rtd-theme furo sphinx-book-theme && apt update && python3 -m pip install j2cli
